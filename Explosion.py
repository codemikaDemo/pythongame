import Settings as S

class Explosion(S.pg.sprite.Sprite):
    def __init__(self, center, size):
        S.pg.sprite.Sprite.__init__(self)
        self.size = size
        self.image = S.explosion_anim[self.size][0]
        self.rect = self.image.get_rect()
        self.rect.center = center
        self.frame = 0
        self.last_update = S.pg.time.get_ticks()
        self.frame_rate = 50

    def update(self):
        now = S.pg.time.get_ticks()
        if now - self.last_update > self.frame_rate:
            self.last_update = now
            self.frame += 1
            if self.frame == len(S.explosion_anim[self.size]):
                self.kill()
            else:
                center = self.rect.center
                self.image = S.explosion_anim[self.size][self.frame]
                self.rect = self.image.get_rect()
                self.rect.center = center
