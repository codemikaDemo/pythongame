import Settings as S

def stones_create():
    for i in range(S.STONE_COUNT):
        Stone()

class Stone(S.pg.sprite.Sprite):
    def __init__(self):
        S.pg.sprite.Sprite.__init__(self)
        self.image_orig = S.STONE_IMG
        self.image = self.image_orig.copy()
        self.rect = self.image.get_rect()
        self.radius = 30
        self.start()
        S. STONES.add(self)
        self.rot = 0
        self.rot_speed = S.rnd.randrange(-10, 10)
        self.last_update = S.pg.time.get_ticks()

    def update(self):
        self.rotate()
        self.rect.x += self.speedx
        self.rect.y += self.speedy
        if self.rect.top > S.HEIGHT + 10:
            self.start()
        if self.rect.left < 0 or self.rect.right > S.WIDTH:
            self.speedx *= -1

    def start(self):
        self.rect.x = S.rnd.randrange(S.WIDTH - self.rect.width)
        self.rect.y = S.rnd.randrange(-200, -self.rect.height)
        self.speedy = S.rnd.randrange(1, 5) * S.STONE_SPEED
        self.speedx = S.rnd.randrange(-5, 5) * S.STONE_SPEED

    def rotate(self):
        now = S.pg.time.get_ticks()
        if now - self.last_update > 50:
            self.last_update = now
            self.rot = (self.rot + self.rot_speed) % 360
            new_image = S.pg.transform.rotate(self.image_orig, self.rot)
            old_center = self.rect.center
            self.image = new_image
            self.rect = self.image.get_rect()
            self.rect.center = old_center