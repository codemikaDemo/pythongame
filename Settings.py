import pygame as pg
import random as rnd
from os import path
pg.init()
pg.mixer.init()

# Цвета игры
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
YELLOW = (255, 255, 0)

# Настройки игры
WIDTH = 1000
HEIGHT = 800
FPS = 30
SCREEN = pg.display.set_mode((WIDTH, HEIGHT))
SPRITES = pg.sprite.Group() # группа всех спрайтов
CLOCK = pg.time.Clock()
pg.display.set_caption("python Game")
RUNNING = True

# Настройки путей к файлам
GAME_FOLDER = path.dirname(__file__)
IMAGES = path.join(GAME_FOLDER, 'images')
background = pg.image.load(path.join(IMAGES, 'background.jpg'))
explosion_anim = {}
explosion_anim['lg'] = []
explosion_anim['sm'] = []
explosion = path.join(IMAGES, 'explosion')
for i in range(9):
    filename = '0{}.png'.format(i)
    print(filename)
    print(IMAGES)
    img = pg.image.load(path.join(explosion, filename)).convert()
    img.set_colorkey(BLACK)
    img_lg = pg.transform.scale(img, (75, 75))
    explosion_anim['lg'].append(img_lg)
    img_sm = pg.transform.scale(img, (32, 32))
    explosion_anim['sm'].append(img_sm)

SOUNDS = path.join(path.dirname(__file__), 'sounds')
SHOOT_SOUND = pg.mixer.Sound(path.join(SOUNDS, 'pew.wav'))
EXPL_SOUNDS = []
for snd in ['expl3.wav', 'expl6.wav']:
    EXPL_SOUNDS.append(pg.mixer.Sound(path.join(SOUNDS, snd)))

# Игрок
PLAYER_IMG = pg.image.load(path.join(IMAGES, 'rocket.png'))
PLAYER_SPEED = 7
PLAYERS = pg.sprite.Group()
PLAYER_LIVES = 5
SCORE = 0

# Камни
STONE_IMG = pg.image.load(path.join(IMAGES, 'stone.png'))
STONE_COUNT = 10
STONES = pg.sprite.Group() # группа всех камней
STONE_SPEED = 1

# Пули
BULLETS = pg.sprite.Group()
BULLET_SPEED = 10
BULLET_DUO = 0