import Settings as S

class Bullet(S.pg.sprite.Sprite):
    def __init__(self, x, y):
        S.pg.sprite.Sprite.__init__(self)
        self.image = S.pg.image.load(S.path.join(S.IMAGES, 'laser.webp'))
        self.rect = self.image.get_rect()
        self.rect.centerx = x
        self.rect.bottom = y
        self.speedy = S.BULLET_SPEED
        S.BULLETS.add(self)

    def update(self):
        self.rect.y -= self.speedy
        if self.rect.bottom < 0:
            self.kill()
        key = S.pg.key.get_pressed()
        if key[S.pg.K_t]:
            S.BULLET_DUO = 40