from Player import *
from Stone import *
from Explosion import *

font_name = S.pg.font.match_font('arial')
def draw_text(surf, text, size, x, y):
    font = S.pg.font.Font(font_name, size)
    text_surface = font.render(text, True, S.WIDTH)
    text_rect = text_surface.get_rect()
    text_rect.midtop = (x, y)
    surf.blit(text_surface, text_rect)

player = Player()
stones_create()

while S.RUNNING:
    S.CLOCK.tick(S.FPS)
    for e in S.pg.event.get():
        if e.type == S.pg.QUIT:
            S.RUNNING = False

    if S.pg.sprite.groupcollide(S.STONES, S.PLAYERS, True, False):
        player.hit()
        stones_create()
    hits = S.pg.sprite.groupcollide(S.STONES, S.BULLETS, True, True)
    for hit in hits:
        S.SCORE += 1
        expl = Explosion(hit.rect.center, 'lg')
        S.rnd.choice(S.EXPL_SOUNDS).play()
        S.SPRITES.add(expl)
        Stone()

    S.SPRITES.add(S.PLAYERS, S.STONES, S.BULLETS)
    S.SPRITES.update()
    S.SCREEN.blit(S.background, (0, 0))
    S.SPRITES.draw(S.SCREEN)
    text = "Баллы: " + str(S.SCORE) + " Жизни: " + str(S.PLAYER_LIVES)
    draw_text(S.SCREEN, text, 25, S.WIDTH / 2, 50)
    S.pg.display.flip()
S.pg.quit()