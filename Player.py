import Bullet as B
import Settings as S

class Player(S.pg.sprite.Sprite):
    def __init__(self):
        S.pg.sprite.Sprite.__init__(self)
        self.image = S.PLAYER_IMG
        self.rect = self.image.get_rect()
        self.radius = 45
        self.speedx = 0
        self.speedy = 0
        self.start()
        self.last_shoot = S.pg.time.get_ticks()
        self.shoot_delay = 200
        S.PLAYERS.add(self)

    def start(self):
        self.rect.centerx = S.WIDTH / 2
        self.rect.bottom = S.HEIGHT - 30

    def update(self):
        self.speedx = self.speedy = 0
        key = S.pg.key.get_pressed()
        if key[S.pg.K_RIGHT]:
            self.speedx = S.PLAYER_SPEED
        if key[S.pg.K_LEFT]:
            self.speedx = -S.PLAYER_SPEED
        if key[S.pg.K_UP]:
            self.speedy = -S.PLAYER_SPEED
        if key[S.pg.K_DOWN] and self.rect.bottom < S.HEIGHT - 30:
            self.speedy = S.PLAYER_SPEED
        if key[S.pg.K_SPACE]:
            self.shoot()
        self.rect.x += self.speedx
        self.rect.y += self.speedy

        if self.rect.left > S.WIDTH:
            self.rect.right = 0
        if self.rect.right < 0:
            self.rect.left = S.WIDTH

    def hit(self):
        if S.PLAYER_LIVES > 1:
            S.PLAYER_LIVES -= 1
            S.SPRITES.remove(S.STONES)
            S.STONES.remove(S.STONES)
            self.start()
        else:
            S.RUNNING = False

    def shoot(self):
        now = S.pg.time.get_ticks()
        if now - self.last_shoot > self.shoot_delay:
            self.last_shoot = now
            S.SHOOT_SOUND.play()
            B.Bullet(self.rect.centerx - S.BULLET_DUO, self.rect.centery)
            B.Bullet(self.rect.centerx + S.BULLET_DUO, self.rect.centery)